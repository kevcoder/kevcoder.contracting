/* eslint-disable no-unused-vars */ 
import dayjs  from "dayjs";
export const __client_data = {
    c: {
        "id": "",
        "name": "",
        "addrLine1": "",
        "addrLine2": "",
        "city": "",
        "state": "",
        "zip": "",
        "contactFirst": "",
        "contactLastName": "",
        "phone": "",
        "email": "",
        "jobs": [],
        "isActive": true

    },
    states: []
};

export const __workItem =
{
  "description": "",
  "billableTime": {
    hours: 0,
    minutes: 0,
    minMinutes: 0,
    minHours: 0
  },
  "creationDt": null,
  isNew: true
};

export const __job = {
    creationDt: dayjs().format('YYYY-MM-DDTHH:mm:ss'),
    name: '',
    description: '',
    workItems: [],
    billRate: 0.00,
    billTypes: 'Hourly',
    dueDt: null,
    clientApprovalDt: null,
    approvedBy: '',
    completionDt: null,
    closedDt: null,
    amountCollected: 0.00
};
